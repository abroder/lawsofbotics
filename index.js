#!/usr/bin/env node

var request = require('request')
var cheerio = require('cheerio')
var twit = require('twit')
var async = require('async')
var fs = require('fs')

var config = require(__dirname + '/config')

var T = new twit(config)
var verbs = new Set(JSON.parse(fs.readFileSync(__dirname + '/lexicon.txt')))

const getRule = function (callback) {
  request('http://www.wikihow.com/Special:Randomizer', (err, res, body) => {
    if (err) {
      callback(err)
      return
    }

    const $ = cheerio.load(body)
    const steps = $('.step b')
    var step = steps[0].children[0].data
    if (step === undefined || step[step.length - 1] != '.') {
      getRule(callback)
      return
    }

    const words = step.split(' ')
    if (!verbs.has(words[0])) {
      console.log(words[0])
      getRule(callback)
      return
    }

    step = 'A bot must ' + ((Math.random() > 0.33) ? '' : 'not ') +
      step[0].toLowerCase() + step.substr(1)
    callback(undefined, step)
  })
}

const generate = function () {
  async.parallel([getRule, getRule, getRule], (err, results) => {
    if (err) {
      console.log(err)
      return
    }

    var tweet = results.join('\n')

    if (tweet.length > 140) {
      generate()
    } else {
      T.post('statuses/update', { status: tweet }, (err, data, res) => {
        if (err) {
          console.log(err)
          return
        }
      })
    }
  })
}

generate()
