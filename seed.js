var request = require('request')
var cheerio = require('cheerio')
var async = require('async')
var fs = require('fs')

const getRule = function (callback) {
  request('http://www.wikihow.com/Special:Randomizer', (err, res, body) => {
    if (err) {
      callback(err)
      return
    }

    const $ = cheerio.load(body)
    const steps = $('.step b')
    if (steps[0] === undefined || steps[0].children[0] === undefined) {
      getRule(callback)
      return
    }

    const step = steps[0].children[0].data
    if (step === undefined || step[step.length - 1] != '.') {
      getRule(callback)
      return
    }

    const tokens = step.split(' ')
    callback(null, tokens[0])
  })
}

async.timesLimit(1500, 50, (n, next) => { getRule(next) }, (err, words) => {
  const verbs = new Set(words)
  fs.writeFileSync('lexicon.txt', JSON.stringify([...verbs])) 
})